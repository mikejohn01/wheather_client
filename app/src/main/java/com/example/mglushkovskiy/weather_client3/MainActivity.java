package com.example.mglushkovskiy.weather_client3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.mglushkovskiy.weather_client3.WeatherFragment;


public class MainActivity extends AppCompatActivity {

    WeatherFragment wf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new WeatherFragment())
                    .commit();
        }

        //wf.updateWeatherData(new CityPreference(getActivity()).getCity()); //нет класса CityPreference.
        wf.updateWeatherData(new City(wf.getActivity()).getCity());

    }











}
